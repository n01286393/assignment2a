﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="csharp.aspx.cs" Inherits="assignment2a.csharp" %>
<asp:Content runat="server" ContentPlaceHolderID="concept">
    <h1>Fundamental concepts to understand in C#</h1>
    <div class="row">
      <div class="col-md-3">
            <h2>What's an object?</h2>
                <p>Remember: a program is a bunch of objects working and interacting with eachother. 
                These objects have their own characteristics and attributes. They also have their ownabilities and behaviors.
                Thinking about software this way is beneficial because you can break down the problem into different parts, 
                craft each of those objects one at a time and put them back together. An object is simply an instance of a class. 
                </p>
       </div>
        <div class="col-md-3">
            <h2>What's a class?</h2>
                <p>In a netshell, classes are a software design pattern. A class is a 
                template/blue-print for making individual objects of a particular type 
                (i.e. like a cookie cutter, it defines the shape of a type of object, its methods and properties). 
                Each individual object that that class makes is called an "instance" of that class.
                </p>
        </div>
        <div class="col-md-3">
            <h2>What's a subclass?</h2>
                <p>Class in C# is short for classification. Classes can have more refined classifications 
                within them, called subclasses. Subclasses inherit the attributes and behaviors of the more general classes.
                </p>
        </div>
        <div class="col-md-3">
            <h2>What's inheritance?</h2>
                <p>Many objects in the real world often share the same attributes and behaviors. For example,
                an animal can either be a vertebrate or an invertebrate. Though these two categories are separate they
                inherit the attributes and behaviors of their parent class of 'animal'. The new class inherites methods 
                and properties from its parent class. This allows us to build slightly different data types 
                from existing data types with relatively little work.
                </p>
                <p>Inheritance allows us to create new types that inherit the attributes and behaviors of existing types.
                </p>
        </div>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="mycodesnippet" runat="server">
    <h3>My basic code snippet illustrating classes and objects:</h3>
        <p>This is my code snippet illustrating a basic media library program to store my favourite books and movies.
        I have two classes for my program: book and movie. In my <code>Main()</code> method I used the <code>new</code> 
        keyword to create at least one instance of each of my media types.
        </p>
        <uctrl:Codebox runat="server" codeType="csharp" codeSource="me"></uctrl:Codebox>
</asp:Content>
<asp:Content ContentPlaceHolderID="outsidecodesnippet" runat="server">
    <h3>An outside code snippet illustrating inheritance:</h3>
        <h4>This code is taken from Kirti Mangal writing for the website GeeksForGeeks. You can find the code snippet live 
            <a href="https://www.geeksforgeeks.org/c-inheritance/" target="_blank">here.</a>
        </h4>
        <uctrl:Codebox runat="server" codeType="csharp" codeSource="web"></uctrl:Codebox>
</asp:Content>
<asp:Content ContentPlaceHolderID="helpfullinks" runat="server">
    <h3>Here are some helpful links with C# and these larger OOP concepts:</h3>
        <ul class="pl-0">
            <li class="listStyle">
                <i class="fab fa-microsoft"></i>
                    <a href="https://stackify.com/oop-concepts-c-sharp/" target="_blank">Stackify: OOP Concepts in C#: Code Examples and How to Create a Class</a>
            </li>
            <li class="listStyle">
                <i class="fab fa-microsoft"></i>
                    <a href="https://code.visualstudio.com/docs/languages/csharp" target="_blank">Visual Studio Documentation</a>
            </li>
            <li class="listStyle">
                <i class="fab fa-microsoft"></i>
                <a href="https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/inheritance" target="_blank">Microsoft Documentation: Explaining Inheritance</a>
            </li>
        </ul>
</asp:Content>