﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="CSS.aspx.cs" Inherits="assignment2a.CSS" %>
<asp:Content runat="server" ContentPlaceHolderID="concept">
    <h1>CSS Positioning</h1>
        <p>The <code>position</code> CSS property specifies how an element is positioned in your page. 
        Positioning content and elements on a page bring structure to designs and help make content for disgestable.
        There are four main position declarations: <code> position: relative; position: absolute; position: static;</code> and <code>position: fixed;</code>.
        </p>
        <p>In order to have complete control over styling the elements of your document, you need to understand how these positions work.
        </p>
            <div class="row">
                <div class="col-md-3">
                    <h2>position: static</h2>
                        <p>HTML elements are positioned static by default. It will always be positioned according to the natural flow of the page.</p>
                </div>
                <div class="col-md-3">
                    <h2>position: relative</h2>
                        <p>Elements with <code>position: relative</code> is positioned relative to its normal position. What 
                            this means is "relative to itself". If you set <code>position: relative;</code> on an element 
                            but no other positioning attributes, nothing will change. But if you dogive it some other positioning like <code>top: 50px;</code>, 
                            it will shift its position 50 pixels down from where it would normally be.
                        </p>
                </div>
                <div class="col-md-3">
                    <h2>position: absolute</h2>
                        <p>This position allows you to place any page element exacly where you want it. 
                        It will be positioned relative to the nearest positioned ancestor; however, if it has no positioned ancestors it will default all the way up to the <code>html</code> element, 
                        meaning it will be placed relative to the page.
                        </p>
                </div>
                <div class="col-md-3">
                    <h2>position: fixed</h2>
                        <p>An element with this position is positioned relative to the viewport, which means that it will always stay 
                        in the same place even if the page is scrolled.
                        </p>
                </div>
            </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="mycodesnippet" runat="server">
    <h2>Absolute Positioning Inside Relative Positioning</h2>
        <p>A page element with relative positioning gives you control to absolutely position children elements inside of it. 
        To me, this was a huge "Ah-ha!" moment that took a while to understand.
        </p>
            <figure>
                 <figcaption>Relative and Absolute positioning demonstrated</figcaption>
                    <p>In the outer box, the position is set to relative and its inner contents are set to absolute. 
                    This is very powerful as it allows you to place any page element exacly where you want it.
                    </p>
                    <div id="box1">
                            <div id="minyBox">position: absolute;
                            top: 0;
                            bottom: 0;
                            </div>
                        <div id="outsideBox">position: absolute;
                            bottom: 10px;
                            right: -30px;
                        </div>
                    </div>
            </figure>
</asp:Content>
<asp:Content ContentPlaceHolderID="outsidecodesnippet" runat="server">
    <h2>Positioning with floats</h2>
        <h3>This code is by Shay Howe. You can find the article 
            <a href="https://learn.shayhowe.com/html-css/positioning-content/" target="_blank">here</a>.
        </h3>
            <p>Another way to position elements on a page is with the <code>float</code> property. The <code>float</code> property allows us to take an element, remove it from the normal flow of a page
            and position it to the left or right of its parent element. All other elements on the page will then flow around the parent element.
            </p>
            <uctrl:Codebox runat="server" codeType="css" codeSource="web" SkinId="codebox-light"></uctrl:Codebox>
          
            
</asp:Content>
<asp:Content ContentPlaceHolderID="helpfullinks" runat="server">
     <h3>Here are some helpful links for CSS:</h3>
        <ul class="pl-0">
            <li class="listStyle"><i class="fab fa-css3-alt"></i>
                <a href="https://css-tricks.com/" target="_blank">CSS Tricks</a>
            </li>
            <li class="listStyle"><i class="fab fa-css3-alt"></i>
                <a href="https://www.w3schools.com/css/default.asp" target="_blank">W3schools</a>
            </li>
            <li class="listStyle"><i class="fab fa-css3-alt"></i>
                <a href="https://tympanus.net/codrops/" target="_blank">CoDrops: a web design and development blog</a>
            </li>
        </ul>
</asp:Content>