﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="javascript.aspx.cs" Inherits="assignment2a.javascript" %>

<asp:Content runat="server" ContentPlaceHolderID="concept">
   <h1>Javascript Arrays</h1>
        <p>Arrays are one of the most useful data structures in Javascript. They are the best way to group similar data together in a sequence 
        (basically a list of information). You can store any JS data type as an item in your array (i.e. a string, a number, an object and even 
        another array). To identify the position of an item in an array, Javascript uses integers called indices. The first item in an array is at 0, 
        so on and so forth.
        </p>
        <p>To use an array effectively, you need to have a way of looking through them to see the data they hold. 
        This action is called iteration and Javascript provides many methods for iterating over an array. 
        </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="mycodesnippet" runat="server">
    <h2>For loop vs forEach method</h2>
    <uctrl:Codebox runat="server" codeType="js" codeSource="me"></uctrl:Codebox>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="outsidecodesnippet">
        <h2>Outside Code Snippet that demonstrates reduce method</h2>
            <h3>This code is provided by Josh Pitzali. You can read the article 
                <a href="https://medium.freecodecamp.org/reduce-f47a7da511a9" target="_blank">here</a>.
            </h3>
            <uctrl:Codebox runat="server" codeType="js" codeSource="web"></uctrl:Codebox>
</asp:Content>
<asp:Content ContentPlaceHolderID="helpfullinks" runat="server">
    <h3>Here are some helpful links for Arrays in Javascript:</h3>
        <ul class="pl-0">
            <li class="listStyle"><i class="fab fa-js"></i>
                <a href="https://eloquentjavascript.net/04_data.html" target="_blank">Eloquent Javascript: Data Structures</a>
            </li>
            <li class="listStyle"><i class="fab fa-js"></i>
                <a href="https://www.youtube.com/watch?v=BMUiFMZr7vk&list=PL0zVEGEvSaeEd9hlmCXrk5yUyqUag-n84" target="_blank">YouTube Channel all about Functional Programming in Javascript</a>
            </li>
            <li class="listStyle"><i class="fab fa-js"></i>
                <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array" target="_blank">Mozilla: More Array Iteration Methods</a>
            </li>
        </ul>
</asp:Content>