﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment2a.UserControls
{   
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string CodeType
        {
            get { return (string)ViewState["codeType"]; }
            set { ViewState["codeType"] = value; }
        }
        
        public string CodeSource
        {
            get { return (string)ViewState["codeSource"]; }
            set { ViewState["codeSource"] = value; }
        }
    
        DataView CreateCodeSource()
        {
            DataTable codedata = new DataTable();
            DataColumn index_col = new DataColumn();
            DataColumn code_col = new DataColumn();
            DataRow coderow;

            index_col.ColumnName = "Line";
            index_col.DataType = Type.GetType("System.Int32");
            codedata.Columns.Add(index_col);

            code_col.ColumnName = "Code";
            code_col.DataType = Type.GetType("System.String");
            codedata.Columns.Add(code_col);
            
            List<string> my_javascript_code = new List<string>(new string[]{
                "//A simple for loop:",
                "const groceries = ['carrot', 'cucumber', 'zuchinni', 'sweet potato', 'artichoke', 'milk', 'bread', 'eggs', 'cereal', 'banana'];",
                "",
                "for (let i = 0; i < groceries.length;i++) {",
                "~console.log(groceries[i]);",
                "}",
                "",
                "//The forEach method is simply a loop that accesses each item in an array. It has a shorter and clearer syntax than the classic for loop.",
                "const groceries = ['carrot', 'cucumber', 'zuchinni', 'sweet potato', 'artichoke', 'milk', 'bread', 'eggs', 'cereal', 'banana'];",
                "",
                "groceries.forEach(groceryItem => console.log(groceryItem));",
                "",
                "const prices = [0.67, 67.89, 55.88, 45];",
                "let total = 0;",
                "",
                "prices.forEach(price => {",
                "~total += price;", 
                "});",
                "",
                "console.log('Your total comes to: ' + total + '$');",
                "",
                "// Output:",                    
                "// Your total comes to: 169.44$"
            });

            List<string> outside_js_code = new List<string>(new string[]{
                "//Outside code demonstrating more useful Javascript methods, specifically reduce.",
                "//You would use this method if you have an array of amounts and you want to add them together, find the average and create a tally of the array.",
                "",
                "const euros = [29.76, 41.85, 46.5];",                
                "const sum = euros.reduce((total, amount) => total + amount);",            
                "sum // 118.11",               
                "const euros = [29.76, 41.85, 46.5];",               
                "const average = euros.reduce((total, amount, index, array) => {",
                "~total += amount;",
                "~if( index === array.length-1) {", 
                "~~return total/array.length;",
                "~}else {",
                "~~return total;",
                "~}",
                "});",           
                "average // 39.37",             
                "const fruitBasket = ['banana', 'cherry', 'orange', 'apple', 'cherry', 'orange', 'apple', 'banana', 'cherry', 'orange', 'fig' ];",
                "const count = fruitBasket.reduce( (tally, fruit) => {",
                "~tally[fruit] = (tally[fruit] || 0) + 1 ;",
                "~return tally;",
                "} , {})",               
                "count // { banana: 2, cherry: 3, orange: 3, apple: 2, fig: 1 }"
            });
            
            List<string> outside_css_code = new List<string>(new string[]{
                "Howe's code to represent a two column layout with float",
                "code {",
                "~background: #2db34a;",
                "~border-radius: 6px;",
                "~color: #fff;",
                "~display: block;",
                "~font: 14px/24px \"Source Code Pro\", Inconsolata, \"Lucida Console\", Terminal, \"Courier New\", Courier;",
                "~padding: 24px 15px;",
                "~text-align: center;",
                "}",
                "",
                "header,",
                "section,",
                "aside,",
                "footer {",
                "~margin: 0 1.5% 24px 1.5%;",
                "}",
                "",
                "section {",
                "~float: left;",
                "~width: 63%;",
                "}",
                "",
                "aside {",
                "~float: right;",
                "~width: 30%;",
                "}",
                "",
                "footer {",
                "~clear: both;",
                "~margin-bottom: 0;",
                "}"          
            });
            
            List<string> my_csharp_code = new List<string>(new string[]{
                "using System;",
                "namespace Alexis.MediaLib",
                "{",
                "~class Program",
                "~{",
                "~~static void Main()",
                "~~{",
                "~~~var book = new Book(\"Everything is Illuminated\", \"2002\");",
                "~~~var movie = new Movie(\"Blade Runner\", \"1982\");",
                "~~~Console.WriteLine(\"Book: \" + book.Title + \" published in \" + book.Year);",
                "~~~Console.WriteLine(\"Movie: \" + movie.Title + \" released in \" + movie.Year);",
                "~~}",
                "~}",
                "}",
                "//Here are my two media types classes.",
                "//For each, I have a constructor function.", 
                "//I've also defined a constructor parameter for each of its fields and initialized them. Imagine them in separate class files.",
                "//Book class",
                "namespace Alexis.MediaLib",
                "{",
                "~class Book", 
                "~{",
                "~~public string Title;", 
                "~~public string Year;", 
                "~~public Book(string title, string year)",
                "~~{",
                "~~~Title = title",
                "~~~Year = year;",
                "~~}",
                "~}",
                "}",
                "//Movie Class",
                "namespace Alexis.MediaLib",
                "{",
                "~class Movie",
                "~{",
                "~~public string Title;",
                "~~public string Year;",
                "~~public Movie(string title, string year)",
                "~~{",
                "~~~Title = title;",
                "~~~Year = year;",
                "~~}",
                "}"              
            });
            
            List<string> outside_csharp_code = new List<string>(new string[]{
                "//Below is an example of inheritance. Class GFB is the base class,", 
                "//class GeeksforGeeks is a derived class which extends GFG class.", 
                "Class Sudo is a driver class to run the program.",
                "",
                "using System;", 
                "namespace ConsoleApplication1 { ",
                "// Base class",
                "class GFG {",
                "~// data members",
                "~public string name;",
                "~public string subject;",
                "~// public method of base class", 
                "~public void readers(string name, string subject)", 
                "~{", 
                "~~this.name = name;", 
                "~~this.subject = subject;",
                "~~Console.WriteLine(\"Myself: \" + name);",  
                "~~Console.WriteLine(\"My Favorite Subject is: \" + subject);", 
                "~}",
                "}", 
                "// inheriting the GFG class using :",  
                "class GeeksforGeeks : GFG {",
                "~~// constructor of derived class", 
                "~~public GeeksforGeeks()", 
                "~~{", 
                "~~~Console.WriteLine(\"GeeksforGeeks\");", 
                "~~}", 
                "}",  
                "// Driver class", 
                "class Sudo {", 
                "",
                "~~// Main Method", 
                "~~static void Main(string[] args)", 
                "~~{", 
                "~~~// creating object of derived class",
                "~~~GeeksforGeeks g = new GeeksforGeeks();", 
                "~~~// calling the method of base class",  
                "~~~// using the derived class object", 
                "~~~g.readers(\"Kirti\", \"C#\");", 
                "~~}", 
                "}", 
                "}",
                "Output:",                
                "//GeeksforGeeks",
                "//Myself: Kirti",
                "//My Favorite Subject is: C#"
            });
            

            List<string> buildCode = new List<string>();
            if( CodeType == "css" ){
                buildCode = outside_css_code;
            } else if ( CodeType == "js" ){
                if ( CodeSource == "web" ){
                    buildCode = outside_js_code;
                } else if ( CodeSource == "me" ){
                    buildCode = my_javascript_code;
                }
            } else if ( CodeType == "csharp" ){
                if ( CodeSource == "web" ){
                    buildCode = outside_csharp_code;
                } else if ( CodeSource == "me" ){
                    buildCode = my_csharp_code;
                }
            }
            

            int i = 1;
            foreach (string code_line in buildCode)
            {
                coderow = codedata.NewRow();
                coderow[index_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }

            DataView codeview = new DataView(codedata);
            return codeview;
        
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {   
            DataView ds = CreateCodeSource();
            codebox.DataSource = ds;
            codebox.DataBind();
        }
    
    
    }
}
